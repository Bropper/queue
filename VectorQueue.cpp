#include "VectorQueue.h"

VectorQueue::VectorQueue(const ValueType* rawArray, const size_t size, float coef)
{
	this->_data = new ValueType[size];
	for (size_t i = 0; i < size; ++i)
	{
		this->_data[i] = rawArray[i];
	}
	this->_size = size;
	this->_capacity = size;
	this->_multiplicativeCoef = coef;
}

VectorQueue::~VectorQueue()
{
	delete[] this->_data;
	this->_size = 0;
	this->_capacity = 0;
}

VectorQueue::VectorQueue(const VectorQueue& other)
{
	if (this->_data != other._data){
		this->_data = new ValueType[other._size];
		this->_capacity = other._size;
		this->_size = other._size;
		for (size_t i = 0; i < this->_size; ++i)
		{
			this->_data[i] = other._data[i];
		}
	}
}

VectorQueue& VectorQueue::operator=(const VectorQueue& other)
{
	if (this->_data != other._data){
		delete [] this->_data;
		this->_capacity = other._size;
		this->_size = other._size;
		this->_data = new ValueType[other._size];
		for (size_t i = 0; i < _size; ++i)
		{
			this->_data[i] = other._data[i];
		}
	}
	return *this;
}

VectorQueue::VectorQueue(VectorQueue&& other) noexcept
{
	if (this->_data != other._data){
		this->_size = other._size;
		this->_capacity = other._size;
		this->_data = other._data;
		other._data = nullptr;
		other._capacity = 0;
		other._size = 0;
	}
}

VectorQueue& VectorQueue::operator=(VectorQueue&& other) noexcept
{
	if (this->_data != other._data){
		delete [] this->_data;
		this->_size = other._size;
		this->_capacity = other._size;
		this->_data = other._data;
		other._data = nullptr;
		other._capacity = 0;
		other._size = 0;
	}
	return *this;
}

size_t VectorQueue::size() const
{
	return this->_size;
}

ValueType VectorQueue::pop_front()
{
	
	if (this->_size == 0)
	{
		throw this->_size;
	}
	ValueType* newData = new ValueType[this->_capacity];
	ValueType tmp = this->_data[0];
	this->_size--;
	for (size_t i = 0; i < this->_size; ++i)
	{
		newData[i] = this->_data[i + 1];
		
	}
	this->_data = newData;
	return tmp;
}

void VectorQueue::push(const ValueType& value)
{
	if (this->_size == this->_capacity)
	{
		if (this->_capacity == 0)
		{
			this->_capacity = 1 * _multiplicativeCoef;
			ValueType* newData = new ValueType[this->_capacity];
			newData[this->_size] = value;
			this->_size += 1;
			this->_data = newData;
		}
		else
		{
			this->_capacity *= this->_multiplicativeCoef;
			ValueType* newData = new ValueType[this->_capacity];
			for (size_t i = 0; i < this->_size; ++i)
			{
				newData[i] = this->_data[i];
			}
			newData[_size] = value;
			this->_size += 1;
			this->_data = newData;
		}
	}
	else
	{
		this->_data[_size] = value;
		this->_size += 1;
	}
}

bool VectorQueue::isEmpty() const
{
	if (this->_size == 0)
	{
		return true;
	}
	return false;
}

const ValueType& VectorQueue::top() const
{
	if ( _size == 0)
		throw -1;
	return this->_data[0];
}

const ValueType& VectorQueue::end() const
{
	if ( _size == 0)
		throw -1;
	return this->_data[_size - 1];
}
