#include "ListQueue.h"
#include <iostream>

ListQueue::Node::Node(ValueType val)
{
    value = val;
    next = nullptr;
}

size_t ListQueue::size() const
{
    return _count;
}

bool ListQueue::isEmpty() const
{
    return _count == 0;
}

const ValueType& ListQueue::top() const
{
	if ( isEmpty() )
		throw -1;
	Node* tmp = _head;
	ValueType mass[_count];
	for (int i = 0; i < _count; ++i)
	{
		mass[i] = tmp->value;
		tmp = tmp->next;
	}
	Node* tmp1 = new Node(mass[_count - 1]);
	return tmp1->value;
}

const ValueType& ListQueue::end() const
{
	if ( isEmpty() )
		throw -1;
	return _head->value;
}

ValueType ListQueue::pop_front()
{
   if ( isEmpty() )
		throw -1;
	Node* tmp = _head;
	ValueType mass[_count];
	for (int i = 0; i < _count; ++i)
	{
		mass[i] = tmp->value;
		tmp = tmp->next;
	}
	Node* tmp1 = new Node(mass[_count - 1]);
	_count--;
	return tmp1->value; 
}

void ListQueue::push(const ValueType& value)
{
    Node* node = new Node(value);
    if ( isEmpty() )
    {
        this->_head = node;
        this->_count++;
    }
    else
    {
        node->next = _head;
        this->_head = node;
        this->_count++;
    }
}

ListQueue::ListQueue(const ValueType* rawArray, const size_t size)
{
    while ( _count != size )
    {
        this->push(rawArray[_count]);
    }
}

ListQueue::~ListQueue()
{
      while ( _count != 0 )
    {
        Node* next = _head->next;
        delete _head;
        _head = next;
        _count--;
    }
}

ListQueue::ListQueue(const ListQueue& other)
{
	
	if ( other.isEmpty() )
	{
		_head = nullptr;
		_count = 0;
		return;
	}
	Node* tmp = other._head;
	ValueType mass[other._count];
	for (int i = 0; i < other._count; ++i)
	{
		mass[i] = tmp->value;
		tmp = tmp->next;
	}
	for ( int i = other._count - 1; i >= 0; --i )
	{
		this->push(mass[i]);
	}
}

ListQueue& ListQueue::operator=(const ListQueue& other)
{
	if ( other.isEmpty() )
	{
		_head = nullptr;
		_count = 0;
		return *this;
	}
	Node* tmp = other._head;
	ValueType mass[other._count];
	for (int i = 0; i < other._count; ++i)
	{
		mass[i] = tmp->value;
		tmp = tmp->next;
	}
	for ( int i = other._count - 1; i >= 0; --i )
	{
		this->push(mass[i]);
	}
	return *this;
}

ListQueue::ListQueue(ListQueue&& other) noexcept
{
	if ( other.isEmpty() )
	{
		_head = nullptr;
		_count = 0;
		return;
	}
	Node* tmp = other._head;
	while ( tmp != nullptr )
	{
		this->push(tmp->value);
		tmp = tmp->next;
		other.pop_front();
	}
}

ListQueue& ListQueue::operator=(ListQueue&& other) noexcept
{
	if ( other.isEmpty() )
	{
		_head = nullptr;
		_count = 0;
		return *this;
	}
	Node* tmp = other._head;
	while ( tmp != nullptr )
	{
		this->push(tmp->value);
		tmp = tmp->next;
		other.pop_front();
	}
	return *this;
}
