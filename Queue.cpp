#include "Queue.h"
#include "ListQueue.h"
#include "VectorQueue.h"
#include "QueueImplementation.h"

#include <stdexcept>

Queue::Queue(QueueContainer container)
    : _containerType(container)
{
    switch (container)
    {
    case QueueContainer::List: 
	{
        _pimpl = static_cast<IQueueImplementation*>(new ListQueue());    // конкретизируйте под ваши конструкторы, если надо
        break;
    }
    case QueueContainer::Vector: 
	{
        _pimpl = static_cast<IQueueImplementation*>(new VectorQueue());    // конкретизируйте под ваши конструкторы, если надо
        break;
    }
    default:
	{
        throw std::runtime_error("Неизвестный тип контейнера");
	}
    }
}

Queue::Queue(const ValueType* valueArray, const size_t arraySize, QueueContainer container)
{
    // принцип тот же, что и в прошлом конструкторе
	switch (container)
    {
    case QueueContainer::List: 
	{
        _pimpl = static_cast<IQueueImplementation*>(new ListQueue(valueArray, arraySize));    // конкретизируйте под ваши конструкторы, если надо
        break;
    }
    case QueueContainer::Vector: 
	{
        _pimpl = static_cast<IQueueImplementation*>(new VectorQueue(valueArray, arraySize));    // конкретизируйте под ваши конструкторы, если надо
        break;
    }
    default:
	{
        throw std::runtime_error("Неизвестный тип контейнера");
	}
    }
}

Queue::Queue(const Queue& copyQueue)
{
	if (this != &copyQueue)
	{
		*this = copyQueue;	
	}
}

Queue& Queue::operator=(const Queue& copyQueue)
{
   if( &copyQueue == this ) { 
	    return *this;
    }
    this->_containerType = copyQueue._containerType;
    delete _pimpl;
    switch ( this->_containerType ) {
		case QueueContainer::List: {
    			  ListQueue* tmpThis = new ListQueue( *static_cast<ListQueue*>(copyQueue._pimpl) );
		          this->_pimpl = static_cast<IQueueImplementation*>(tmpThis);
			  break;
		}	
		case QueueContainer::Vector: {
			VectorQueue* tmpThis = new VectorQueue( *static_cast<VectorQueue*>(copyQueue._pimpl) );
		        this->_pimpl = static_cast<IQueueImplementation*>(tmpThis);
			break;
		}
    	    	default:
		    throw std::runtime_error("Неизвестный тип контейнера");
    }
    return *this;
}

Queue::~Queue()
{
    delete _pimpl;        // композиция!
}

void Queue::push(const ValueType& value)
{
    // можно, т.к. push определен в интерфейсе
    _pimpl->push(value);
}

ValueType Queue::pop_front()
{
    return _pimpl->pop_front();
}

const ValueType& Queue::top() const
{
    return _pimpl->top();
}

const ValueType& Queue::end() const
{
    return _pimpl->end();
}

bool Queue::isEmpty() const
{
    return _pimpl->isEmpty();
}

size_t Queue::size() const
{
    return _pimpl->size();
}
