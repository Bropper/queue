#pragma once
// Уровень абстракции
// клиентский код подключает именно этот хедер
#include <stdlib.h>
// тип значений в стеке
using ValueType = double;

// на основе какого контейнера работает стек
enum class QueueContainer {
    Vector = 0,
    List,
    // можно дополнять другими контейнерами
};

// декларация интерфейса реализации
class IQueueImplementation;

class Queue
{
public:
    // Большая пятерка
    Queue(QueueContainer container = QueueContainer::Vector);
    // элементы массива последовательно подкладываются в стек
    Queue(const ValueType* valueArray, const size_t arraySize, 
          QueueContainer container = QueueContainer::Vector);

    explicit Queue(const Queue& copyQueue);
    Queue& operator=(const Queue& copyQueue);

    // Здесь как обычно
	Queue(Queue&& moveQueue) noexcept;
    Queue& operator=(Queue&& moveQueue) noexcept;

    ~Queue();

    // добавление в хвост
    void push(const ValueType& value);
    // удаление с начала
    ValueType pop_front();
    // посмотреть элемент в начале
    const ValueType& top() const;
	// посмотреть элемент в хвосте
	const ValueType& end() const;
    // проверка на пустоту
    bool isEmpty() const;
    // размер 
    size_t size() const;
private:
    // указатель на имплементацию (уровень реализации)
    IQueueImplementation* _pimpl = nullptr;
    // тип контейнера, наверняка понадобится
    QueueContainer _containerType;
};
