#pragma once
#include "QueueImplementation.h"
#include <stdlib.h>

/// type of vector item
/// TODO: change to template
using ValueType = double;

class VectorQueue : public IQueueImplementation
{
public:
    // All c-tors and "=" operators make vectors 
    // where _capacity is equal to _size
    VectorQueue() = default;
    VectorQueue(const ValueType* rawArray, const size_t size, float coef = 2.0f);

    explicit VectorQueue(const VectorQueue& other);
    VectorQueue& operator=(const VectorQueue& other);

    explicit VectorQueue(VectorQueue&& other) noexcept;
    VectorQueue& operator=(VectorQueue&& other) noexcept;

	~VectorQueue();

    void push(const ValueType& value);
    ValueType pop_front();
	const ValueType& top() const;
	const ValueType& end() const;
	bool isEmpty() const;
    size_t size() const;

private:
    ValueType* _data = nullptr;
    size_t _size = 0;
    size_t _capacity = 0;
    float _multiplicativeCoef = 2.0f;
};
