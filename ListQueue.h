#pragma once
#include <stdlib.h>
#include "QueueImplementation.h"
using ValueType = double; 

class ListQueue : public IQueueImplementation
{


public:
	ListQueue() = default;
	ListQueue(const ValueType* rawArray, const size_t size);

    explicit ListQueue(const ListQueue& other);
    ListQueue& operator=(const ListQueue& other);

    explicit ListQueue(ListQueue&& other) noexcept;
    ListQueue& operator=(ListQueue&& other) noexcept;

	~ListQueue();

    void push(const ValueType& value);
    ValueType pop_front();
	const ValueType& top() const;
	const ValueType& end() const;
	bool isEmpty() const;
    size_t size() const;

private:
	struct Node
	{
		ValueType value;
		Node* next;
		Node(ValueType val);
	};

	Node* _head = nullptr;
	size_t _count = 0;
};
